-- Gera��o de Modelo f�sico
-- Sql ANSI 2003 - brModelo.



CREATE TABLE Bens (
Desc_breve VARCHAR(10),
Categoria NUMERIC(10),
Desc_completa VARCHAR(10),
Lote INTEGER
)

CREATE TABLE Leilao (
Preco_min DECIMAL(10),
Lote INTEGER PRIMARY KEY,
User_id INTEGER
)

CREATE TABLE User (
CPF_CNPJ VARCHAR(10),
Categoria CHAR(10),
Nome VARCHAR(10),
EMAIL VARCHAR(10),
User_id INTEGER PRIMARY KEY
)

CREATE TABLE Lance (
Lote INTEGER,
User_id INTEGER,
Valor DECIMAL(10),
PRIMARY KEY(Lote,User_id)
)

ALTER TABLE Bens ADD FOREIGN KEY(Lote) REFERENCES Leilao (Lote)
ALTER TABLE Leilao ADD FOREIGN KEY(User_id) REFERENCES User (User_id)
