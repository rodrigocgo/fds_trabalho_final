--Cria��o das tabelas

CREATE TABLE Bens (
Desc_breve VARCHAR(50) NULL,
Categoria VARCHAR(30) NOT NULL,
Desc_completa VARCHAR(150) NOT NULL,
Lote INTEGER NOT NULL
);

CREATE TABLE Leilao (
Preco_min DECIMAL(10) NOT NULL,
Lote INTEGER PRIMARY KEY,
Status VARCHAR(50) NOT NULL,
CPF_CNPJ VARCHAR(50) 
);

CREATE TABLE Usuario (
CPF_CNPJ VARCHAR(50) PRIMARY KEY,
Nome VARCHAR(100) NOT NULL,
Email VARCHAR(50) NOT NULL,
Categoria VARCHAR(50) NOT NULL
);

CREATE TABLE Lance (
Lote INTEGER,
Lance_id INTEGER,
CPF_CNPJ VARCHAR(50),
Valor DECIMAL(10) NOT NULL,
PRIMARY KEY(Lote, Lance_id))
;



--Adi��o de FKs
ALTER TABLE Bens ADD FOREIGN KEY(Lote) REFERENCES Leilao (Lote);
ALTER TABLE Leilao ADD FOREIGN KEY(CPF_CNPJ) REFERENCES Usuario (CPF_CNPJ);



--Popula��o das tabelas
insert into Usuario (CPF_CNPJ, NOME, EMAIL, CATEGORIA) values (
'934.549.613-00', 'Andr� dos Santos', 'andre_santos@gmail.com','C'
);

insert into Usuario (CPF_CNPJ, NOME, EMAIL, CATEGORIA) values (
'835.504.228-06', 'Evandro Souza', 'evandro_souza@gmail.com', 'L'
);

insert into Usuario (CPF_CNPJ, NOME, EMAIL, CATEGORIA) values (
'646.801.830-71', 'Eduarda Teixeira', 'eduarda_teixeira@hotmail.com', 'C'
);

insert into Usuario (CPF_CNPJ, NOME, EMAIL, CATEGORIA) values (
'271.164.526-64', 'Carolina Dantas', 'carolina_dantas@yahoo.com', 'L'
);

insert into Leilao (PRECO_MIN, LOTE, STATUS, CPF_CNPJ) values (
50000, 1, 'A', '646.801.830-71'
);

insert into Bens values (
'Vaso de diamantes e ouro', 'Rel�quias', 
'Um vaso raro feito artesanalmente com diamantes e ouro h� aproximadamente 200 anos 
em uma pequena cidade no extremo sul da Dinamarca', 1
);

insert into Lance (LOTE, LANCE_ID, CPF_CNPJ, VALOR) values (
1, 1, '646.801.830-71', 51000
);



