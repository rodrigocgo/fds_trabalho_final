package UInterface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Business.Leilao;

public class TelaConsultaLeilao extends JFrame
{

  private JPanel contentPane;
  private JTextField txtLote;

  /**
   * Launch the application.
   */
  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          TelaConsultaLeilao frame = new TelaConsultaLeilao();
          frame.setVisible(true);
        } catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    });
  }

  /**
   * Create the frame.
   */
  public TelaConsultaLeilao()
  {
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    setBounds(100, 100, 232, 160);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(null);
    
    JLabel lblNewLabel = new JLabel("Numero Lote");
    lblNewLabel.setBounds(10, 11, 110, 14);
    contentPane.add(lblNewLabel);
    
    txtLote = new JTextField();
    txtLote.setBounds(10, 36, 86, 20);
    contentPane.add(txtLote);
    txtLote.setColumns(10);
    
    JButton bntProcura = new JButton("Procurar");
    bntProcura.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) 
      {
        String sLote;
        sLote = txtLote.getText();
        try
        {
          LeilaoFacade leilaoFacade = new LeilaoFacade();
          leilaoFacade.buscarLeilao(Integer.parseInt(sLote));
          TelaDisplayErro erro = new TelaDisplayErro();
          Leilao leilao = leilaoFacade.buscarLeilao(Integer.parseInt(sLote));
          erro.SetAviso("Preco min: " + leilao.getPrecoMinimo()  + "\n Lote: " +leilao.getLote() + "\n Status: " + leilao.getStatus() + "\n Documento: " + leilao.getCPF_CNPJ());
          erro.setVisible(true);        
         
        }
        catch(Exception e1)
        {
          TelaDisplayErro erro = new TelaDisplayErro();
          erro.SetAviso(e1.getMessage());
          erro.setVisible(true);
        }
      }
    });
    bntProcura.setBounds(117, 87, 89, 23);
    contentPane.add(bntProcura);
    
    JButton btnEncerrar = new JButton("Encerrar");
    btnEncerrar.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) 
      {
        String sNumeroLote;
        sNumeroLote = txtLote.getText();
        
        try
        {
          LeilaoFacade leilao = new LeilaoFacade();
          leilao.encerraLeilaoLote(Integer.parseInt(sNumeroLote));
         
        }
        catch(Exception e1)
        {
          TelaDisplayErro erro = new TelaDisplayErro();
          erro.SetAviso(e1.getMessage());
          erro.setVisible(true);
        }
      }
    });
    btnEncerrar.setBounds(7, 87, 89, 23);
    contentPane.add(btnEncerrar);
  }
}
