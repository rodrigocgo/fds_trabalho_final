package UInterface;

import Business.Bem;
import Business.BemService;
import Business.BemServiceImpl;
import Business.Lance;
import Business.LanceService;
import Business.LanceServiceImpl;
import Business.Leilao;
import Business.LeilaoService;
import Business.LeilaoServiceImpl;
import Business.Usuario;
import Business.UsuarioService;
import Business.exceptions.InvalidLanceException;
import Business.exceptions.InvalidUsuarioException;
import Business.exceptions.LeilaoEncerradoException;
import Business.exceptions.NoLeilaoFoundException;
import Business.exceptions.NoUserFoundException;
import Business.exceptions.PersistenceException;

import java.util.List;

public class LeilaoFacade {

    protected LeilaoService leilaoServ;
    protected BemService bemServ;
    protected LanceService lanceServ;
    protected UsuarioService userServ;

    public LeilaoFacade() throws ClassNotFoundException {
        leilaoServ = new LeilaoServiceImpl();
        bemServ = new BemServiceImpl();
        lanceServ = new LanceServiceImpl();
    }

    /**
     * Busca um leilao pelo numero do lote
     *
     * @param lote lote do leilao
     * @return objeto Leilao encontrado
     * @throws NoLeilaoFoundException caso nao exista leilao com o lote
     * informado
     * @throws Business.exceptions.NoUserFoundException caso algum lance possuir
     * usuario invalido
     */
    public Leilao buscarLeilao(Integer lote) throws NoLeilaoFoundException, NoUserFoundException {
        Leilao leilao = leilaoServ.buscarLeilaoByLote(lote);
        //popularInfos(leilao);
        return leilao;
    }

    /**
     * Lista todos Leiloes em aberto
     *
     * @return Lista com leiloes abertos
     * @throws Business.exceptions.NoLeilaoFoundException caso algum leilao
     * esteja inconsistente na base de dados
     * @throws Business.exceptions.NoUserFoundException caso algum usuario de
     * algum lance sej ainválido
     */
    public List<Leilao> buscarLeiloesAbertos() throws NoLeilaoFoundException, NoUserFoundException {
        List<Leilao> result = leilaoServ.buscarLeiloesAbertos();
        for (Leilao leilao : result) {
            popularInfos(leilao);
        }
        return result;
    }

    /**
     * Lista todos leiloes encerrados
     *
     * @return Lista com leiloes encerrados
     * @throws Business.exceptions.NoLeilaoFoundException caso algum leilao
     * esteja inconsistente na base de dados
     * @throws Business.exceptions.NoUserFoundException caso algum usuario de
     * algum lance sej ainválido
     */
    public List<Leilao> buscarLeiloesEncerrados() throws NoLeilaoFoundException, NoUserFoundException {
        List<Leilao> result = leilaoServ.buscarLeiloesEncerrados();
        for (Leilao leilao : result) {
            popularInfos(leilao);
        }
        return result;
    }

    /**
     * Adiciona um Lance ao Leilao
     *
     * @param leilao Leilao que terá lance adicionado
     * @param valor VAlor do lance
     * @param user Usuario que fez o lance
     * @throws InvalidLanceException Caso o valor seja inválido para o lance
     */
    public void darLance(Leilao leilao, Float valor, Usuario user) throws InvalidLanceException {
        Lance lance = lanceServ.darLance(user, leilao, valor);
        leilao.getLances().add(lance);
    }
    
    /**
     * Remove o lance informado
     *
     * @param lance lance a ser removido
     * @param leilao leilao que contem o lance removido
     * @throws InvalidLanceException CAso o lance não seja encontrado, id
     * inválido
     */
    public void removerLance(Lance lance, Leilao leilao) throws InvalidLanceException {
        lanceServ.removerLance(lance);
        leilao.getLances().remove(lance);
    }

    /**
     * Criar um novo leilao para o usuario
     *
     * @param usuario
     * @param precoMinimo
     * @return
     * @throws Business.exceptions.InvalidLanceException quando o preço for
     * invalido
     * @throws Business.exceptions.NoUserFoundException quando o leilão não
     * possuir um criador associado
     * @throws Business.exceptions.InvalidUsuarioException usuario criador não é
     * da categoria leiloeiro "L"
     */
    public Leilao criarLeilao(String doc, Double precoMinimo, Integer lote) throws InvalidLanceException, NoUserFoundException, InvalidUsuarioException {
        Leilao leilao = new Leilao();
        leilao.setCPF_CNPJ(doc);
        leilao.setPrecoMinimo(precoMinimo);
        leilao.setStatus("Aberto");
        leilao.setLote(lote);
        return leilaoServ.gerarLeilao(leilao);
    }

    /**
     * Adiciona um bem a um leilao
     *
     * @param leilao leilao que receberá o bem
     * @param descricaoBreve descrição breve do bem
     * @param categoria categoria do bem
     * @param descricaoCompleta descrição completa do bem
     */
    public void adicionarBemAoLeilao(Leilao leilao, String descricaoBreve, String categoria, String descricaoCompleta) {
        Bem bem = new Bem();
        bem.setCategoria(categoria);
        bem.setDescBreve(descricaoBreve);
        bem.setDescCompleta(descricaoCompleta);
        bemServ.adicionarBem(bem, leilao);
    }

    /**
     * Remove um bem de um leilao
     *
     * @param bem bem a ser removido
     * @param leilao leilao que contem o bem
     */
    public void removerBem(Bem bem, Leilao leilao) {
        bemServ.removerBem(bem, leilao);
    }

    private void popularInfos(Leilao leilao) throws NoLeilaoFoundException, NoUserFoundException {
        leilao.setBens(bemServ.buscarBensByLeilao(leilao));
        leilao.setLances(lanceServ.buscarLancesByLeilao(leilao));
    }
    
    public void encerraLeilaoLote(int lote) throws LeilaoEncerradoException, PersistenceException, ClassNotFoundException
    {
      leilaoServ = new LeilaoServiceImpl();
      leilaoServ.encerraLeilaoPorLote(lote);
    }
}
