/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.List;

public interface BemService {

    void adicionarBem(Bem bem, Leilao leilao);

    void removerBem(Bem bem, Leilao leilao);

    List<Bem> buscarBensByLeilao(Leilao leilao);
}
