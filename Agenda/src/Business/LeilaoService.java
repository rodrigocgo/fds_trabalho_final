/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.InvalidLanceException;
import Business.exceptions.LeilaoEncerradoException;
import Business.exceptions.NoLeilaoFoundException;
import Business.exceptions.NoUserFoundException;
import Business.exceptions.PersistenceException;
import java.util.List;

public interface LeilaoService {

    /**
     * Cadastra um novo leilao
     *
     * @param leilao
     * @return
     * @throws Business.exceptions.InvalidLanceException quando o preço for
     * invalido
     * @throws Business.exceptions.NoUserFoundException quando o leilão não
     * possuir um criador associado
     */
    Leilao gerarLeilao(Leilao leilao) throws InvalidLanceException, NoUserFoundException;

    /**
     * Encerra um leilão aberto
     *
     * @param leilao
     * @throws Business.exceptions.LeilaoEncerradoException Se o Leilao já tiver
     * sido encerrado
     * @throws Business.exceptions.PersistenceException Caso ocorra algum erro
     * ao encerrar o leilao.
     */
    void encerrarLeilao(Leilao leilao) throws LeilaoEncerradoException, PersistenceException;
     
    void encerraLeilaoPorLote(int lote) throws LeilaoEncerradoException, PersistenceException;
    /**
     * Busca um Leilao peloi numero do lote
     * @param lote
     * @return
     * @throws NoLeilaoFoundException Caso não existir nenhum leilao com este numero
     */
    Leilao buscarLeilaoByLote(Integer lote) throws NoLeilaoFoundException;

    List<Leilao> buscarLeiloesAbertos();
    
    List<Leilao> buscarLeiloesEncerrados();

}
