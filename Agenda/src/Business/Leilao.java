/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.exceptions.NoLancesException;
import java.util.Collections;
import java.util.List;

public class Leilao {

    private Usuario criador;
    private Double precoMinimo;
    private Integer lote;
    private List<Bem> bens;
    private List<Lance> lances;
    private String status;
    private String CPF_CNPJ;

    /**
     * Retorna o {@link Lance} atualmente vencendo um leilão.
     *
     * @throws NoLancesException quando o leilao nao possuir lan ces
     * @return
     */
    public Lance getLanceVencedor() throws NoLancesException {
        if (lances.isEmpty()) {
            throw new NoLancesException();
        }
        Collections.sort(lances);
        return lances.get(-1);
    }

    public Usuario getCriador() {
        return criador;
    }

    public void setCriador(Usuario criador) {
        this.criador = criador;
    }

    public Double getPrecoMinimo() {
        return precoMinimo;
    }

    public void setPrecoMinimo(Double precoMinimo) {
        this.precoMinimo = precoMinimo;
    }

    public Integer getLote() {
        return lote;
    }

    public void setLote(Integer lote) {
        this.lote = lote;
    }

    public List<Bem> getBens() {
        return bens;
    }

    public void setBens(List<Bem> bens) {
        this.bens = bens;
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCPF_CNPJ() {
        return CPF_CNPJ;
    }

    public void setCPF_CNPJ(String CPF_CNPJ) {
        this.CPF_CNPJ = CPF_CNPJ;
    }
}
