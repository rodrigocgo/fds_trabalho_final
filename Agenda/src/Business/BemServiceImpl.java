/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Persistence.BemDAO;
import Persistence.BemDAOOracle;
import Persistence.BemTO;
import java.util.ArrayList;
import java.util.List;

public class BemServiceImpl implements BemService {

    protected final BemDAO bemDAO;

    public BemServiceImpl() throws ClassNotFoundException {
        bemDAO = new BemDAOOracle();
    }

    @Override
    public void adicionarBem(Bem bem, Leilao leilao) {

        BemTO to = new BemTO();
        to.setCategoria(bem.getCategoria());
        to.setDesc_breve(bem.getDescBreve());
        to.setDesc_completa(bem.getDescCompleta());
        to.setLote(leilao.getLote());
        bemDAO.adicionar(to);

        leilao.getBens().add(bem);
    }

    @Override
    public void removerBem(Bem bem, Leilao leilao) {

        BemTO to = new BemTO();
        to.setCategoria(bem.getCategoria());
        to.setDesc_breve(bem.getDescBreve());
        to.setDesc_completa(bem.getDescCompleta());
        to.setLote(leilao.getLote());
        bemDAO.remover(to);

        leilao.getBens().remove(bem);
    }

    @Override
    public List<Bem> buscarBensByLeilao(Leilao leilao) {
        List<BemTO> bensTO = bemDAO.buscarBensByLeilao(leilao.getLote());
        List<Bem> result = new ArrayList<>();
        for (BemTO to : bensTO) {
            Bem bem = new Bem();
            bem.setCategoria(to.getCategoria());
            bem.setDescBreve(to.getDesc_breve());
            bem.setDescCompleta(to.getDesc_completa());
            bem.setLote(to.getLote());
            result.add(bem);
        }
        return result;
    }

}
