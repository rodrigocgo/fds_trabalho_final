/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import java.util.List;

public interface BemDAO {
    void adicionar(BemTO bem);
    void remover(BemTO bem);
    List<BemTO> buscarBensByLeilao(Integer lote);
}
