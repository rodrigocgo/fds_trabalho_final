/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

public class LanceTO {

    private Float valor;
    private Integer lote;
    private Integer lanceId;
    private String cpf_cnpj;

    public Integer getLanceId() {
        return lanceId;
    }

    public void setLanceId(Integer lanceId) {
        this.lanceId = lanceId;
    }

    /**
     * @return the valor
     */
    public Float getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(Float valor) {
        this.valor = valor;
    }

    /**
     * @return the lote
     */
    public Integer getLote() {
        return lote;
    }

    /**
     * @param lote the lote to set
     */
    public void setLote(Integer lote) {
        this.lote = lote;
    }

    public String getCpf_cnpj() {
        return cpf_cnpj;
    }

    public void setCpf_cnpj(String cpf_cnpj) {
        this.cpf_cnpj = cpf_cnpj;
    }

    /**
     * @return the usuarioId
     */
 


}
