/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    public static Connection getConnection() throws ClassNotFoundException {
        ConnectionFactory c = new ConnectionFactory();
        String database = "";
        String user = ""; //colocar o usuário do banco utilizado
        String password = ""; //colocar a senha do banco utilizado	

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection con = DriverManager.getConnection(
                    "jdbc:oracle:thin:@//camburi.pucrs.br:1521/facin11g", user, password);

            return con;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
