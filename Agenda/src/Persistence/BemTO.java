/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

public class BemTO {
    
    private String categoria;
    private String desc_completa;
    private String desc_breve;
    private Integer lote;

    
    public BemTO (){
        
    }
    
    public BemTO (String desc_breve){ //verificar se pode fazer sobrecarga no TO
        this.desc_breve = desc_breve;
    }
    
    
    /**
     * @return the lote
     */
    public Integer getLote() {
        return lote;
    }

    /**
     * @param lote the lote to set
     */
    public void setLote(Integer lote) {
        this.lote = lote;
    }

    /**
     * @return the categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the desc_completa
     */
    public String getDesc_completa() {
        return desc_completa;
    }

    /**
     * @param desc_completa the desc_completa to set
     */
    public void setDesc_completa(String desc_completa) {
        this.desc_completa = desc_completa;
    }

    /**
     * @return the desc_breve
     */
    public String getDesc_breve() {
        return desc_breve;
    }

    /**
     * @param desc_breve the desc_breve to set
     */
    public void setDesc_breve(String desc_breve) {
        this.desc_breve = desc_breve;
    }
}
