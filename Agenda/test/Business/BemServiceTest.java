/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Persistence.BemDAO;
import Persistence.BemTO;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author mbalotin
 */
public class BemServiceTest {

    @Mock
    BemDAO dao;

    @InjectMocks
    BemServiceImpl bemService;

    Leilao LEILAO_TEST;
    Bem BEM_TEST;
    int LOTE_TEST;

    public BemServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        LOTE_TEST = 0;
        
        LEILAO_TEST = new Leilao();
        LEILAO_TEST.setCPF_CNPJ("000000");
        LEILAO_TEST.setLote(LOTE_TEST);
        LEILAO_TEST.setPrecoMinimo(1.0);
        LEILAO_TEST.setStatus("A");

        BEM_TEST = new Bem();
        BEM_TEST.setCategoria("categoria");
        BEM_TEST.setDescBreve("DescB");
        BEM_TEST.setDescCompleta("DescC");
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testAdicionarBem() {
        bemService.adicionarBem(BEM_TEST, LEILAO_TEST);
        verify(dao, times(1)).adicionar(anyObject());
    }

    @Test
    public void testRemoveBem() {
        bemService.removerBem(BEM_TEST, LEILAO_TEST);
        verify(dao, times(1)).remover(anyObject());
    }

    @Test
    public void testBuscarBem() {
        List<BemTO> result = new ArrayList<>();
        BemTO to = new BemTO(BEM_TEST.getDescBreve());
        result.add(to);
        when(dao.buscarBensByLeilao(LOTE_TEST)).thenReturn(result);

        List<Bem> test = bemService.buscarBensByLeilao(LEILAO_TEST);
        assertEquals(test.size(), 1);
        verify(dao, times(1)).buscarBensByLeilao(LOTE_TEST);
        assertTrue(test.get(0).getDescBreve().equals(to.getDesc_breve()));
    }

}
