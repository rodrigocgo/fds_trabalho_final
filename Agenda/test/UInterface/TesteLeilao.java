package UInterface;

import org.junit.Before;

import org.junit.Test;

import Business.exceptions.InvalidLanceException;
import Business.exceptions.InvalidUsuarioException;
import Business.exceptions.LeilaoEncerradoException;
import Business.exceptions.NoLeilaoFoundException;
import Business.exceptions.NoUserFoundException;
import Business.exceptions.PersistenceException;
import Business.exceptions.UserAlreadyExistsException;
import java.sql.SQLException;

public class TesteLeilao {

    private LeilaoFacade leilao;
    private UsuarioFacade usuario;

    @Before
    public void Inicializa() throws ClassNotFoundException, UserAlreadyExistsException, InvalidUsuarioException, InvalidLanceException, NoUserFoundException, SQLException {
        leilao = new LeilaoFacade();
        usuario = new UsuarioFacade();
        usuario.criarUsuario("teste", "123", "TESTE@gmail.com", "L");
        leilao.criarLeilao("123", 54.5, 1);

    }

    @Test
    public void TestaCadastro() throws UserAlreadyExistsException, InvalidUsuarioException, SQLException {
        usuario.criarUsuario("teste1", "1234", "TEST1E@gmail.com", "L");
    }

    @Test
    public void TestaBuscaUsuario() throws NoUserFoundException {
        usuario.buscarUsuario("1234");
    }

    @Test
    public void TestaCriaLeilao() throws InvalidLanceException, NoUserFoundException, InvalidUsuarioException {
        leilao.criarLeilao("12345", 54.5, 1);
    }

    @Test
    public void BuscaLeilao() throws NoLeilaoFoundException, NoUserFoundException {
        leilao.buscarLeilao(123);
    }

    @Test
    public void EnceraLeilao() throws InvalidLanceException, NoUserFoundException, InvalidUsuarioException, LeilaoEncerradoException, PersistenceException, ClassNotFoundException {
        leilao.criarLeilao("123456", 54.5, 1);
        leilao.encerraLeilaoLote(123456);
    }

    @Test
    public void TestAdicionaBemAoLeilao() {

    }
}
